using Test

function testPalindromo()
    @test palindromo("") == true
    @test palindromo("ovo") == true
    @test palindromo("joçoj") == true
    @test palindromo("jabanete") == false
    @test palindromo("maoeoam") == true
    @test palindromo("maoekoam") == false
    @test palindromo("MiniEP11") == false
    @test palindromo("A mãe te ama.") == true
    @test palindromo("Passei em MAC0110!") == false
    @test palindromo("iNtErEsTiNg aMaZiNg") == false
    @test palindromo("zawarudo durawaz") == true
    @test palindromo("aAa?  Aáàã!ÀÃÀ:Á-Áa.") == true
    @test palindromo("                       ") == true
    @test palindromo("A filha da xuxa se chama sasha") == false
    @test palindromo("aBcDeFgHiJkLmNoPqRsTuVWxYz!-?:331''''") == false
    @test palindromo("Socorram-me, subi no ônibus em Marrocos!") == true
    @test palindromo("Á.À,Ã;É:Í'Ó-Õ?Ú!Ç Ú--ÕÓÍ:::É;,;,Ã!-?À'''Á!") == true
    @test palindromo("O romano acata amores a damas amadas e Roma ataca o namoro.") == true
    @test palindromo("Luza Rocelina, a namorada do Manuel, leu na moda da Romana: anil é cor azul.") == true
    @test palindromo("O duplo pó do trote torpe de potro meu que morto pede protetor todo polpudo.") == true
    println("OK")
end

function clearString(string)

    plainString = lowercase(string)
    plainString = replace.(plainString, r"[' ']" => "")
    if sizeof(plainString) == 0
        return ""
    end
    plainString = replace.(plainString, r"[.,;:'-?!]" => "")
    plainString = replace.(plainString, r"[áâãà]" => "a")
    plainString = replace.(plainString, r"[éêẽè]" => "e")
    plainString = replace.(plainString, r"[íîĩì]" => "i")
    plainString = replace.(plainString, r"[óôõò]" => "o")
    plainString = replace.(plainString, r"[úûũù]" => "u")
    plainString = replace.(plainString, r"[ç]" => "c")

    return plainString
end

function palindromo(string)

    string = clearString(string)

    if sizeof(string) > 0

        for index in firstindex(string) : div(sizeof(string),2) + 1
            if string[index] != string[sizeof(string) - index + 1]
              return false
            end
        end

    end

    return true

end

testPalindromo()
